function conversieSaptamaniInZile() {
    var saptamani = parseFloat(document.getElementById('saptamani').value);
    if (isNaN(saptamani) || saptamani < 0) {
        document.getElementById('error').innerText = "Te rog introdu un număr valid de săptămâni (non-negativ).";
        return;
    }
    var zile = saptamani * 7;
    document.getElementById('zile').value = zile;
    document.getElementById('error').innerText = "";
}

function conversieZileInSaptamani() {
    var zile = parseFloat(document.getElementById('zile2').value);
    if (isNaN(zile) || zile < 0) {
        document.getElementById('error').innerText = "Te rog introdu un număr valid de zile (non-negativ).";
        return;
    }
    var saptamani = zile / 7;
    document.getElementById('saptamani2').value = saptamani;
    document.getElementById('error').innerText = "";
}

function reset() {
    document.getElementById('saptamani').value = "";
    document.getElementById('zile').value = "";
    document.getElementById('zile2').value = "";
    document.getElementById('saptamani2').value = "";
    document.getElementById('error').innerText = "";
}
